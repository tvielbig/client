#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>

void printMenu() 
{
    printf("Please select one of the following:\n");
    printf("  l)ist:\n");
    printf("  g)et FILE_NAME\n");
    printf("  s)ize FILE_NAME\n");
    printf("  q)uit\n");
    printf("Choice: ");
}


char getChoice() {
    char buf[1000];
    scanf(" %s", buf);
    return buf[0];
}


int main()
{

    struct sockaddr_in sa;
    int sockfd;

    printf("Starting\n");

    printf("Looking up IP address\n");
    struct hostent *he = gethostbyname("65.19.178.177"); //changed this to default to barry's server
    char *ip = he->h_addr_list[0];
    //printf("IP address is %s\n", ip);
    
    printf("Creating structure\n");
    sa.sin_family = AF_INET;
    sa.sin_port = htons(1234); //changed this to default to barry's server port
    //inet_pton(AF_INET, ip, &sa.sin_addr);
    sa.sin_addr = *((struct in_addr *)ip);
    
    printf("Creating socket\n");
    sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sockfd == -1) {
        fprintf(stderr, "Can't create socket\n");
	exit(3);
    }
    printf("Connecting\n");
    int res = connect(sockfd, (struct sockaddr *)&sa, sizeof(sa));
    if (res == -1) {
        fprintf(stderr, "Can't connect\n");
	exit(2);
    }


    char choice;
    char command[100];
    while(1) {
       printMenu();
       choice = getChoice();
       switch(choice)
       {
            case 'l':
                sprintf(command, "LIST");
                send(sockfd, command, strlen(command), 0);
                printf("Well aint broken yet\n");
                printf("Receving response\n");
                char buf[1000];
                size_t size;
                int count = 0;
                while ((size = recv(sockfd, buf, 1000, 0)) > 0)
                {
                   printf("Writing chunk %d of size %d\n", count, size);
                   count++;
                   fwrite(buf, size, 1, stdout);
                }
                close(sockfd);
                break;
            case 'g':   
                break;
            case 's':
                break;
            case 'q':
                exit(0);
                break;
       }
    }
    
}
